package com.longmin.rsocket.demo.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Repository;

@Repository
public class PersonRepository {

    public Person getOne(String id) {
        return new Person(id, "longmin", 18);
    }

    public Person create(Person person) {
        return person;
    }

    @Data
    @AllArgsConstructor
    public static class Person {
        String id;
        String name;
        Integer age;
    }
}
