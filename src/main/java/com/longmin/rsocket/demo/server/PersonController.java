package com.longmin.rsocket.demo.server;

import com.longmin.rsocket.demo.dao.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
@Slf4j
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    @MessageMapping("person")
    public Mono<PersonRepository.Person> getPerson(String id) {
        log.info("get request for id {}", id);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Mono.just(personRepository.getOne(id));
    }

    @MessageMapping("createPerson")
    public Mono<Void> createPerson(PersonRepository.Person person) {
        personRepository.create(person);
        return Mono.empty();
    }

    @MessageMapping("createBatchPersons")
    Flux<PersonRepository.Person> createBatchPersons(Integer batchNumber) {
        log.info("get request to create {} persons", batchNumber);
        return Flux
                .range(1, batchNumber)
                .map(index -> personRepository
                        .create(new PersonRepository
                                .Person(String.valueOf(index), "longmin", 18)))
                .log()
                .onErrorReturn(null);
    }

    @MessageMapping("channel")
    Flux<PersonRepository.Person> tryChannelMode(Flux<PersonRepository.Person> persons) {
        log.info("get request to try channel mode");
        return persons.doOnNext(person -> {
            //TODO any action to the person.
            log.info("get person {}", person.toString());
        }).switchMap(person ->
                // return a new person to client
                Flux.from(Mono.just(new PersonRepository.Person(person.getId() + " --- Modified", person.getName(), person.getAge())))
                        .log()
        );
    }
}
